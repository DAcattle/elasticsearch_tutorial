# Elasticsearch

## 新增一筆Document
在restaurant的Index裡新增一筆Document

**格式**
```
POST /{index}/_doc
{
  "{field_name}": "{value}"
}
```
**範例**
```
POST /restaurant/_doc
{
  "name": "Krusty Krab",
  "description": "A prominent fast food restaurant located in the underwater city of Bikini Bottom."
}
```

## 用ID取得Document
拿到ID為 l4cbUH0BSQ8p1kkagC8X 的Document

**格式**
```
GET /{index}/_doc/{document_id}
```
**範例**
```
GET /restaurant/_doc/l4cbUH0BSQ8p1kkagC8X
```

## 新增Document並指定其ID
跟前面的新增方式是一樣的，只是在URL的結尾加上指定的ID

**格式**
```
POST /{index}/_doc/{document_id}
{
  "{field_name}": "{value}"
}
```
**範例**
```
POST /restaurant/_doc/1
{
  "name": "Krusty Krab",
  "description": "A prominent fast food restaurant located in the underwater city of Bikini Bottom. The signature dish is KRABBY PATTY made from Mr. Krabs's secret formula. Mr. Krabs (Eugene Krabs) owns and operates the restaurant, he's also obsessed with money.",
  "kind": "restaurant"
}
```
```
POST /restaurant/_doc/2
{
  "name": "Chum Bucket",
  "description": "We still don't know Chum Bucket is a restaurant or laboratory. Chum Stick is their main product. Plankton is the onwer of Chum Bucket. He used to date with Betsy Krabs, but she final, but it turns out that Plankton was using her to steal the secret formula.",
  "kind": "restaurant"
}
```
```
POST /restaurant/_doc/3
{
  "name": "Fancy!",
  "description": "Fancy! is an upper-class restaurant. There are cushioned seats alongside tables and a reception area, as well as a long kitchen which can withstand many chefs. There is also a stage in which an imported pianist plays live-action music on the piano.",
  "kind": "restaurant"
}
```

在新增Document的時候如果遇到有相同ID的Document已經存在，會把已經存在的Document給替換掉

**範例**
```
POST /restaurant/_doc/3
{
  "name": "Weenie Hut Jr's",
  "description": "Weenie Hut Jr's is located in Bikini Bottom. It has two other counterparts, Super Weenie Hut Jr's and Weenie Hut General. Weenie Hut Jr's has an animatronic server who works for. It can identify if someone is a \"weenie\" or not.",
  "kind": "weenie"
}
```

## 用_create的方式新增Document
新增前先檢查ID是否存在，如果ID存在則新增失敗

**格式**
```
POST /{index}/_create/{document_id}
{
  "{field_name}": "{value}"
}
```
**範例**

新增失敗
```
POST /restaurant/_create/3
{
  "name": "Fancy!",
  "description": "Fancy! is an upper-class restaurant. There are cushioned seats alongside tables and a reception area, as well as a long kitchen which can withstand many chefs. There is also a stage in which an imported pianist plays live-action music on the piano.",
  "kind": "restaurant"
}
```
新增成功
```
POST /restaurant/_create/4
{
  "name": "Fancy!",
  "description": "Fancy! is an upper-class restaurant. There are cushioned seats alongside tables and a reception area, as well as a long kitchen which can withstand many chefs. There is also a stage in which an imported pianist plays live-action music on the piano.",
  "kind": "restaurant"
}
```

## 用_update的方式更新Document
只更新在Body裡面的特定欄位

**格式**
```
POST /{index}/_update/{document_id}
{
  "doc": {
    "{field_name}": "{value}"
  }
}
```
**範例**
```
POST /restaurant/_update/2
{
  "doc": {
    "kind": "laboratory"
  }
}
```

## 刪除指定ID的Document
刪除ID為l4cbUH0BSQ8p1kkagC8X的Document

**格式**
```
DELETE /{index}/_doc/{document_id}
```
**範例**
```
DELETE /restaurant/_doc/l4cbUH0BSQ8p1kkagC8X
```
## 新增Index

**格式**
```
POST /{index}
```
**範例**
```
POST /test
```

## 刪除Index

**格式**
```
DELETE /{index}
```
**範例**
```
DELETE /test
```

## match_all query
拿到Index底下的所有Document

**格式**
```
GET /{index}/_search
{
  "query": {
    "match_all": {}
  }
}
```
**範例**
```
GET /restaurant/_search
{
  "query": {
    "match_all": {}
  }
}
```

## match query
關鍵字(Eugene Krabs)會被拆成數個單詞(Eugene, Krabs)，搜尋至少有包含一個單詞的Document

**格式**
```
GET /{index}/_search
{
  "query": {
    "match": {
      "description": "{text you wanna search}"
    }
  }
}
```
**範例**
```
GET /restaurant/_search
{
  "query": {
    "match": {
      "description": "Eugene Krabs"
    }
  }
}
```

## match_phrase query
搜尋包含完整字段(Eugene Krabs)的Document

**格式**
```
GET /{index}/_search
{
  "query": {
    "match": {
      "description": "{text you wanna search}"
    }
  }
}
```
**範例**
```
GET /restaurant/_search
{
  "query": {
    "match": {
      "description": "Eugene Krabs"
    }
  }
}
```

## multi_match query
複數欄位的match搜尋

**格式**
```
GET /{index}/_search
{
  "query": {
    "multi_match": {
      "query": "{text you wanna search}", 
      "fields": [
        "{field 1}",
        "{field 2}"
        ]
    }
  }
}
```
**範例**

搜尋description跟kind這兩個欄位裡有restaurant的Document
```
GET /restaurant/_search
{
  "query": {
    "multi_match": {
      "query": "restaurant", 
      "fields": [
        "description",
        "kind"
        ]
    }
  }
}
```

**範例**

複數欄位的match搜尋，description欄位有匹配的Document分數會更高
```
GET /restaurant/_search
{
  "query": {
    "multi_match": {
      "query": "restaurant", 
      "fields": [
        "description^2",
        "kind"
        ]
    }
  }
}
```

**範例**

複數欄位的match phrase搜尋
```
GET /restaurant/_search
{
  "query": {
    "multi_match": {
      "query": "Eugene Krabs", 
      "fields": [
        "description"
        ],
      "type": "phrase"
    }
  }
}
```
## bool query
bool query有4個keyword可以使用
  must: 符合條件的才納入搜尋結果，會影響score的計算
  must_not: 符合條件的會被移出搜尋結果
  should: 符合條件的在score上會有加成，不符合條件的沒有影響
  filter: 符合條件的才納入搜尋結果，不影響score的計算

**格式**
```
GET /restaurant/_search
{
  "query": {
    "bool": {
      "must": [],
      "must_not": [], 
      "should": [], 
      "filter": []
    }
  }
}
```
**範例**
```
GET /restaurant/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "description": "krabs"
            
          }
        }
      ]
    }
  }
}
```
```
GET /restaurant/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "description": "krabs"
            
          }
        }
      ],
      "must_not": [
        {
          "term": {
            "description": "plankton"
          }
        }
      ]
    }
  }
}
```
```
GET /restaurant/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "description": "krabs"
            
          }
        }
      ],
      "should": [
        {
          "term": {
            "kind": "restaurant"
          }
        }
      ]
    }
  }
}
```
```
GET /restaurant/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "description": "krabs"
            
          }
        }
      ],
      "filter": [
        {
          "term": {
            "kind": "restaurant"
          }
        }
      ]
    }
  }
}
```

# Elasticsearch - Mapping

## dynamic mapping
